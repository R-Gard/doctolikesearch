import {
  BrowserRouter,
  Switch,
  Route,
} from "react-router-dom";
import './styles/app.scss';
// import Home from "./pages/Home";
import Search from "./pages/Search";
import Navbar from './components/Navbar';

function App() {
  return (
    <BrowserRouter>
      <Navbar/>
      <Switch>
        <Route path="/">
          <Search />
        </Route>
        {/* <Route path="/">
          <Home />
        </Route> */}
      </Switch>
    </BrowserRouter>
  );
}

export default App;
