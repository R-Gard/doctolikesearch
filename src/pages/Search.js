import React, { Fragment, useState } from 'react';
import { Container, Stack } from 'react-bootstrap';
import SearchBar from '../components/SearchBar';
import SearchFilter from '../components/SearchFilter';
import SearchResultCard from '../components/SearchResultCard';
import dataDoctors from '../data/doctor';

const Search = () => {
  const [search, setSearch] = useState({name:"", where:""});

  const filterBy = (doctor) => {
    // TODO ADD FILTERS FROM <SEARCHFILTER/>
    if(
      doctor.name.toLowerCase().includes(search.name?.toLowerCase()) ||
      doctor.city.toLowerCase().includes(search.where?.toLowerCase())
    ){
      return doctor;
    }
  }

  return (
    <Fragment>

      <SearchBar search={search} setSearch={setSearch}/>
      
      <SearchFilter />
      
      <Container>
        {/* TODO DYNAMIC H1 BASED ON INPUT SEARCH AND DISPO FILTER */}
        <h1 className="result-title">Trouvez un médecin généraliste (ou un professionnel pratiquant des actes de médecine générale) et réservez en ligne</h1>
        
        <Stack gap={3}>
          {search.name === "" && search.where === ""? (
          dataDoctors.slice(0,5).map((doctor, i) => (
            <SearchResultCard key={i} doctor={doctor}/>
          ))
          ):(
            dataDoctors.filter(filterBy).map((doctor, i) => (
              <SearchResultCard key={i} doctor={doctor}/>
            ))
          )}
          </Stack>
      </Container>
    </Fragment>
  );
};

export default Search;