import React from 'react';
import { Link } from "react-router-dom";
import { Form, InputGroup, FormControl, Container } from 'react-bootstrap';
import { FaSearch, FaAngleRight, FaMapMarkerAlt, FaCrosshairs } from 'react-icons/fa';

const SearchBar = ({search, setSearch}) => {
  return (
    <section id="searchbar-bg" className="primary-bg d-flex align-items-center d-none d-sm-block">
      <Container>
        <Form className="">
          <InputGroup size="larger">
            <InputGroup.Text id="faSearchPreprend"><FaSearch /></InputGroup.Text>
            <FormControl 
              value={search.name} 
              onChange={ (e)=>setSearch({name : e.target.value}) } 
              placeholder="Médecin, établissement, specialité..." 
              aria-label="Doctor" 
              aria-describedby="faSearchPreprend" 
            />

            <InputGroup.Text id="faMapPreprend"><FaMapMarkerAlt /></InputGroup.Text>
            <FormControl  
              value={search.where} 
              onChange={ (e)=>setSearch({where : e.target.value}) } 
              placeholder="Ou ?" 
              aria-label="Where" 
              aria-describedby="faMapPreprend" 
            />
            <InputGroup.Text><FaCrosshairs /></InputGroup.Text>

            <Link to="#" id="searchbar-btn" className="btn">Rechercher <FaAngleRight /></Link>
            
            {/* TODO ROUTING WITH DATA FROM INPUT */}
            {/*
            <Link 
              to={{ 
                pathname: '/search',
                search: '',
                query: { the: 'query' } 
              }} 
              id="searchbar-btn"
              className="btn"
            >
            Rechercher <FaAngleRight />
            </Link>
            */}
           

          </InputGroup>
        </Form>
      </Container>
    </section>
  );
};

export default SearchBar;