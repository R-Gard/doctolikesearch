import React from 'react';
import { Row, Col, Card } from 'react-bootstrap';
import Calendar from '../Calendar';
import Presentation from './Presentation';

const SearchResultCard = ({doctor}) => {
  return (
      <Card className="p-3">
        <Row>
          <Col xs={12} md={3}>
            <Presentation doctor={doctor}/>
          </Col>
          <Col md={9} className="d-none d-sm-block">
            <Calendar doctor={doctor}/>
          </Col>
        </Row>
      </Card>
  );
};

export default SearchResultCard;