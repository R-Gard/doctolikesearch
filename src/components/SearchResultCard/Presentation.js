import React from 'react';
import { Stack } from 'react-bootstrap';
import { FaVideo } from 'react-icons/fa'
import { Link } from 'react-router-dom';

const Presentation = ({doctor}) => {
  return (
    <Stack gap={3}>
        <Stack direction="horizontal">
          <img className="doctor-photo me-3" src={doctor.img} alt="doctorname" />
          <div>
            {/* TODO Prefix: Dr | Mme | M. */}
            <div className="doctor-name">{doctor.name}</div>
            <div className="doctor-role">{doctor.role}</div>
          </div>
        </Stack>

      {doctor.isVideoAvailable && (
        <div style={{ marginLeft: "96px"}}>
          <div><span className="video-bg"><FaVideo size={9}/></span> Consultation vidéo disponible</div>
          <div><Link to="#" className="fw-bold text-decoration-none">En savoir plus</Link></div>
        </div>
      )}

      <div style={{ marginLeft: "96px"}}>
        <div>Adresse</div>
        <div>CP {doctor.city}</div>
      </div>
      
      {doctor.isSector1 && (
        <div style={{ marginLeft: "96px"}} className="doctor-convention"> {/* IF DISPO CONVENTION */}
          Conventionné secteur 1
        </div>
      )}

      <div className="d-flex justify-content-center">
        <Link to="#" className="btn invert2">PRENDRE RENDEZ-VOUS</Link>
      </div>
    </Stack>
  );
};

export default Presentation;