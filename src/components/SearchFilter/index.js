import React from 'react';
import { ButtonGroup, Dropdown, Container, Button } from 'react-bootstrap';
import { FaAngleDown } from 'react-icons/fa';

const FiltersData = [
  {
    name: "Disponibilités",
    options: ["Aujourd'hui", "Dans les trois prochains jours"]
  },
  {
    name: "Honoraires",
    options: ["Sans dépassements d'honoraires", "Dépassements d'honoraires encadrés possibles"]
  },
  {
    name: "Motif de consultation",
    options: ["Vidéo - Patient suivi", "Vidéo - Nouveau patient", "Premiere consultation de médecine générale", "Médecine générale", "Pédiatrie", "Gériatrie", "Médecine du sport", "Homéopathie", "Acupuncture", "Médecine esthétique", "Urgence", "Injection Covid"]
  },
  {
    name: "Langue parlées",
    options: ["Allemand", "Anglais", "Arabe", "Espagnol", "Italien", "Langue des signes", "Polonais", "Portugais", "Romain", "Russe", "Turc"]
  },
  {
    name: "Consultation vidéo",
    options: ["Disponible"]
  }
];


// TODO 
// STATE Filters
// D.Item onCLick -> 
// If already SELECTED reset
// Else SELECT

const SearchFilter = () => {
  const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
    <div
      style={{alignSelf:"center"}}
      ref={ref}
      onClick={e => {
        e.preventDefault();
        onClick(e);
      }}
    > 
      {children}
      <FaAngleDown className="ms-2" color={"#107aca"}/>
    </div>
  ));

  return (
    <div id="filter-bg" className="w-100 sticky-top">
      <Container className="d-flex align-items-baseline">
        <div role="group" className="filter-title btn-group me-2">FILTRER PAR</div>

        {FiltersData.map((item, index) => (

         <Dropdown key={index} as={ButtonGroup} className="btn">
          <Dropdown.Toggle as={CustomToggle}>
           {item.name}
          </Dropdown.Toggle>

          <Dropdown.Menu>
          {(item.options).map((opt, i) => (
              <Dropdown.Item 
                key={i} 
                eventKey={i}
                as={Button}
                onClick={(e)=>console.log(e.target.outerText)}
                className="p-3"
                >
                  {opt}
              </Dropdown.Item>
            ))}
          </Dropdown.Menu>
        </Dropdown>
        
        ))}
      </Container>
    </div>
  );
};

export default SearchFilter;