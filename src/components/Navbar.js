import React from 'react';
import { FaRegUser, FaRegQuestionCircle } from 'react-icons/fa';
import { Link } from 'react-router-dom';

const Navbar = () => {
  return (
    <nav className="primary-bg py-1">
      <div className="d-flex justify-content-between px-md-4">
        <ul className="list-unstyled list-inline mb-0">
          <li className="list-inline-item">
            <Link to="/">
              <div id="nav-logo"></div>
            </Link>
          </li>
        </ul>
        <ul className="d-flex align-items-center gap-md-4 list-unstyled mb-0">
          <li className="list-inline-item">
            <Link to="#" className="btn invert1">Vous etes professionnel de santé ?</Link>
          </li>
          <li className="list-inline-item px-2">
            <Link to="#"><FaRegQuestionCircle className="d-none d-sm-inline"/> Besoin d'aide ?</Link>
          </li>
          <li className="list-inline-item px-2">
            <Link to="#" className="d-inline-flex align-items-center">
              <FaRegUser className="d-none d-sm-inline me-2"/>
              <div>
                <div>Se connecter</div>
                <div id="nav-rdv">Gerer mes RDV</div>
              </div>
            </Link>
          </li>
        </ul>
      </div>
    </nav>
  );
};

export default Navbar;