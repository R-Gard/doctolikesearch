import React, { Fragment, useState, useEffect } from 'react';
import { Stack, Button } from 'react-bootstrap';
import { FaAngleLeft, FaAngleRight, FaEye } from 'react-icons/fa';
import { Link } from 'react-router-dom';
import { dateParser, getTimeTravelDay, getWeek, getHours, randomNum } from '../../services/dateLib';
import Day from './Day';

//TODO RESPONSIVE CROP DAY BY DAY

const Calendar = ({doctor}) => {
  const today = new Date();
  const [week, setWeek] = useState(getWeek(today));
  const [weekIndex, setWeekIndex] = useState(1);
  const [available, setAvailable] = useState({
    weeks: [],
    itemsToShow: 4,
    expanded: false,
    noResult: false
  });

  // available.weeks[weekindex] //[{},{},{},{},{},{},{}]

  const prevWeek = () => {
    // console.log("prev week", weekIndex);
    if (weekIndex === 1) return;
    setWeek(getWeek(getTimeTravelDay(getTimeTravelDay(today, 7 * (weekIndex - 1)), 7, false)));
    setWeekIndex(weekIndex - 1);
  }

  const nextWeek = () => {
    // console.log("next week", weekIndex);
    setWeek(getWeek(getTimeTravelDay(today, 7 * weekIndex)));
    setWeekIndex(weekIndex + 1);
  }

  const showMore = () => {
    // console.log("Show more", available.weeks);
    available.expanded === false ? (
      // TODO itemsToShow: available.weeks[weekindex].custommaxlength
      setAvailable({ ...available, itemsToShow: 10, expanded: true })
    ) : (
      setAvailable({ ...available, itemsToShow: 4, expanded: false })
    );
  }

  //DYNAMIC TIME DATA -> NEVER WRITE JSON AGAIN
  const testPopulate = () =>{
    if(available.weeks[weekIndex-1]) return;

    let populate = {
      ...available  
    }
    let weeks = [];
    week.map((day)=>(
      weeks.push({date: dateParser(day), still: getHours()})
    ));

    populate.weeks.push(weeks);
    
    // TODO DATA WITH NO HOURS
    let random = [true, false];
    setAvailable({...populate, noResult:random[randomNum(1)]});
  }
  
  useEffect(()=>{
    testPopulate();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [weekIndex]);

  return (
    <Fragment>
      <div className="container-calendar-result">
        <div className="calendar">
          <Stack direction="horizontal" gap={2} className="justify-content-between">
            <div onClick={prevWeek} className="align-self-baseline"><FaAngleLeft size={24} color={weekIndex === 1 ? "#D9DEE3" : "#107aca"} /></div>

            {week.map((day, i) => (
              <Day key={i} day={day} available={available} weekIndex={weekIndex} />
            ))}

            <div onClick={nextWeek} className="align-self-baseline"><FaAngleRight size={24} color={"#107aca"} /></div>
          </Stack>

          {available.expanded !== true && (
            <div className="d-flex flex-column justify-content-center align-items-center">
              <hr className="w-100" />
              <Button onClick={() => showMore()}>VOIR PLUS D'HORAIRES</Button>
            </div>
          )}
        </div>
        
        {/* TODO LINK TO NEXT DISPO (WEEKINDEX.result > 0)*/}
        {available.noResult === true && (
          <div className="calendar-overlay">
              <div className="calendar-overlay-msg d-flex">
                <img className="calendar-overlay-img" src="https://www.doctolib.fr/webpack/482189de00daffc04163bf8017cb30b4.png" alt="calendar" /> 
                <div className="calendar-overlay-bg d-flex flex-row align-items-center gap-2 p-3">
                  <div><b>Pas de disponibilité cette semaine</b></div>
                  <div style={{transform: "scale(1.5)"}}>|</div>
                  <Link to="#"><span><FaEye className="me-2"/></span><b>Prochain RDV le ??</b></Link>
                </div>
              </div>
          </div>
        )}

      </div>
    </Fragment>
  );
};

export default Calendar;