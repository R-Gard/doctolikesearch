import React from 'react';
import { Stack } from 'react-bootstrap';
import { parseDayName, parseDayDate, dateParser } from '../../services/dateLib';
import { FaMinus } from 'react-icons/fa';

const Day = ({ day, available, weekIndex}) => {
  let dayName = parseDayName(day);
  let dayDate = parseDayDate(day);
  
  const drawHour = (creneau) => {
    let html = [];

    for (let i = 0; i < available.itemsToShow; i++) {
      if (creneau && creneau[i] !== undefined) {
        html.push(
          <div
            key={i}
            className="day-slots fill"
            aria-label={`${dayName} ${dayDate}`}
            title={`${dayName} ${dayDate}`}
            onClick={() => alert(`Form Confirmation RDV : ${dayName} ${dayDate} ${creneau[i]}`)}
          >
            {creneau[i]}
          </div>
        );
      } else {
        html.push(<div key={i} className="day-slots"> <FaMinus color={"#D9DEE3"} /> </div>);
      }
    }
    return html;
  }

  return (
    <div className="flex-grow-1 align-self-baseline">

      <div className="text-center mb-4">
        <div className="f-upper fw-bold">{dayName}</div>
        <div>{dayDate}</div>
      </div>

      <Stack gap={2}>

        {available.weeks[weekIndex-1] && (
          available.weeks[weekIndex-1].map((creneau) => (
            dateParser(day) === creneau.date && (
              drawHour(creneau.still)
            )
          ))
          )
        }

      </Stack>

    </div>
  );
};

export default Day;