const dateParser = (date) => {
  let newDate = new Date(date).toLocaleDateString("fr-FR", {
    year: "numeric",
    month: "numeric",
    day: "numeric",
  });
  return newDate;
};

const parseDayName = (date) => {
  let dayName = date.toLocaleDateString("fr-FR", {
    weekday: "long"
  });
  return dayName;
};

const parseDayDate = (date) => {
  let dayDate = date.toLocaleDateString("fr-FR", {
    day: "numeric",
    month: "short"
  });
  return dayDate;
}

const getTimeTravelDay = (date, offset = 1, isFuture = true) => {
  let destinationDay = new Date(date);
  if (isFuture) {
    destinationDay.setTime(destinationDay.getTime() + 24 * 3600 * (offset * 1000));
  } else {
    destinationDay.setTime(destinationDay.getTime() - 24 * 3600 * (offset * 1000));
  }
  return destinationDay;
}

const getWeek = (date) => {
  let weekDates = [date];
  for (let i = 1; i < 7; i++) {
    weekDates[i] = getTimeTravelDay(weekDates[weekDates.length - 1]);
  }
  return weekDates;
}

// let date = new Date(); //TODO min 8h - max 20h
// incr 15/30/60
// start min 8h - max 20h
const getHours = (incr=15, max=10, start=null) => {
  let creneaux = []

  let minutes = [15, 30, 60];
  incr = minutes[randomNum(2)];

  let randomMax = randomNum(max);
  
  // let date = new Date(specificHour());
  let date = new Date(new Date().setHours(randomNum(15,8), 0, 0));

  for (let i = 0; i < randomMax; i++) {
    creneaux.push( addMinutes(date, incr) );
  }

  return creneaux;
}

function addMinutes(date, minutes) {
    return new Date(date.setMinutes(date.getMinutes() + minutes))
      .toLocaleTimeString("fr-FR", {
        hour: "2-digit",
        minute: "2-digit",
      }
    );
}

function randomNum(max, min=0){
  return Math.round((Math.random()*max) + min)
}

export { dateParser, parseDayName, parseDayDate, getTimeTravelDay, getWeek, getHours, randomNum }